import React from 'react';
import { StyleSheet, Text, View, Button} from 'react-native';

const GameOverScreen = (props) => {
  return <View style={styles.screen}>
            <Text>The Game is Over!</Text>
            <Text>Number of rounds : {props.rounds}</Text>
            <Button title='New Game' onPress={props.onRestart}/>
        </View>
}

const styles = StyleSheet.create({
    screen: {
        flex:1,
        alignItems: 'center',
        justifyContent: 'center'
    }
});

export default GameOverScreen;